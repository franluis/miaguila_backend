# API "MI AGUILA BACKEND"

Este repositorio contiene el código de la API - Microservicio para la obtención de la información de códigos postales mediante la carga y procesamiento de coordenadas.

# Configuración de despliegue y desarrollo

## Variables de entorno

Estas son las variables de entorno que se pueden configurar para esta API:

Requeridas:

- **URL_API_POSTCODES_IO**: URL base de la API para consulta de codigos postales. Ejemplo: https://postcodes.io/.
- **USER_DATABASE**: Nombre de usuario de la base de datos. Ejemplo: FRANCISCO.
- **PASSWORD_DATABASE**: Clave de acceso a la base de datos para el **USER_DATABASE**. Ejemplo: toor.
- **HOST_DATABASE**: URL donde se encuentra ejecutandose la base de datos. Ejemplo: https://localhost.com/.
- **PORT_DATABASE**: Numero del puerto donde se encuentra ejecutandose la base de datos. Ejemplo: 8081.
- **NAME_DATABASE**: Nombre de la base de datos a utilizar para la API. Ejemplo: db_proyecto.

Opcionales:

- **SERVER_PORT**: Puerto HTTP que utiliza el servidor para exponer los endpoints REST. Predeterminado: 8080.

Para entornos de desarrollo se puede utilizar un archivo `.env` en la base del proyecto con las opciones deseadas. Ver [.env.sample](.env.sample) como ejemplo.

## Scripts

- `npm run dev` Inicia servidor en modo desarrollo con refresco automático ante cambios
- `npm run start` Inicia servidor
- `npm run test` Ejecuta los test unitarios del proyecto