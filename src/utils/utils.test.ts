/**
 * Copyright (C) 2020
 *
 * Autor: Francisco Gerardino
 *
 */

// Importacion de librerias
import { Util } from './utils'

describe('Utils', () => {
    // Verificacion de instancia de la clase Utils
    test('Instancia correctamente de la clase Utils', async done => {
        const obj: Util = new Util()
        expect(obj).toBeDefined()
        done()
    })

    // Verificacion del metodo randomNameFile de la clase Utils
    test('Invocacion de metodo randomNameFile de la clase Utils', async done => {
        const obj: Util = new Util()
        const cadena: string = obj.randomNameFile(5)
        expect(obj).toBeDefined()
        expect(cadena).toBeDefined()
        done()
    })

    // Verificacion del metodo os metodos de la clase Utils
    test('Invocacion de metodo connection de la clase Utils', async done => {
        const obj: Util = new Util()
        const conexion: string = obj.connection()
        expect(obj).toBeDefined()
        expect(conexion).toBeDefined()
        expect(conexion).toEqual(`postgres://${process.env.USER_DATABASE}:${process.env.PASSWORD_DATABASE}@${process.env.HOST_DATABASE}:${process.env.PORT_DATABASE}/${process.env.NAME_DATABASE}`)
        done()
    })
})