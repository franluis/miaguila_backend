/**
 * Copyright (C) 2020
 *
 * Autor: Francisco Gerardino
 *
 */

export class Util {
    // Metodo random de la clase Util
    public randomNameFile(length: number): string {
        const character: string = `${process.env.USER_DATABASE}`
        let stringAux: string = ''
        for (let i = 0; i < length; i++) {
            stringAux += character.charAt(Math.floor(Math.random() * character.length))
        }
        return stringAux
    }

    // Metodo conexion con postgres de la clase Util
    public connection(): string {
        const conexionString: string = `postgres://${process.env.USER_DATABASE}:${process.env.PASSWORD_DATABASE}@${process.env.HOST_DATABASE}:${process.env.PORT_DATABASE}/${process.env.NAME_DATABASE}`
        return conexionString
    }
}