/**
 * Copyright (C) 2020
 *
 * Autor: Francisco Gerardino
 *
 */

// Importacion de librerias
import App from './app'
import dotenv from 'dotenv'

// Inicializacion de las variables del entorno desde .env
dotenv.config()

const port = process.env.SERVER_PORT || 8080
// Instancia de la clase App
const objApp = new App()

// Inicializacion del servidor HTTP
objApp.app.listen(port, () => {
    console.log(`API - MI AGUILA BACKEND inicializada en http://localhost:${port}`)
})