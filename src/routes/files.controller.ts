/**
 * Copyright (C) 2020
 *
 * Autor: Francisco Gerardino
 *
 */

// Importacion de librerias
import express from 'express'
import { Util } from '../utils/utils'
import { FileServices } from '../services/file.services'

// Instancia de la clase Util
const objUtil: Util = new Util()

// Instancia de la clase FileServices
const objFileService: FileServices = new FileServices()

export class ControllerFile {
    // Metodo rutas de la clase ControllerFile
    public routes(app: any): void {
        // Endpoint post para carga de archivo
        app.route('/file/upload').post((req: express.Request, res: express.Response) => {
            objFileService.upload(req, res)
        })
        // Endpoint post para lectura del archivo y obtencion de informacion por coordenadas
        app.route('/file/read').post((req: express.Request, res: express.Response) => {
            objFileService.read(req, res)
        })
    }
}