/**
 * Copyright (C) 2020
 *
 * Autor: Francisco Gerardino
 *
 */

// Importacion de librerias
import express from 'express'
import bodyParser from 'body-parser'
import fileUpload from 'express-fileupload'
import { ControllerFile } from './routes/files.controller'

export default class App {
    // Atributos de la clase App
    public app: express.Application
    private routes: ControllerFile = new ControllerFile()

    // Constructor de la clase App
    constructor() {
        this.app = express()
        this.config()
        // Inicializacion de las rutas
        this.routes.routes(this.app)
    }

    // Metodo config de la clase App
    private config(): void {
        this.app.use(bodyParser.urlencoded({ extended: true }))
        this.app.use(bodyParser.json())
        this.app.use(fileUpload())
    }
}
