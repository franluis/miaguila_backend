/**
 * Copyright (C) 2020
 *
 * Autor: Francisco Gerardino
 *
 */

// Importacion de librerias
import async from 'async'
import axios from 'axios'
import express from 'express'
import fs from 'fs'
import { Client } from 'pg'
import { Util } from '../utils/utils'

export class FileServices {
    // Metodo upload de la clase FileServices
    public upload(req: express.Request, res: express.Response): void {
        // Instancia de la clase Util
        const objUtil: Util = new Util()
        // Captura del archivo desde el form-data del request
        const objectFile: any = req.files?.archivo
        // Asignacion de nombre unico al archivo sobre la carpeta public
        const nameFile: string = objUtil.randomNameFile(5) + '_' + objectFile.name
        const target: string = `./public/${nameFile}`
        // Manejo de Excepcion
        try {
            async.series({
                uploadFile: (callback: any) => {
                    objectFile.mv(target, (errorCarga: any) => {
                        if (errorCarga) {
                            res.status(500).json({
                                status: 500,
                                message: errorCarga
                            })
                        }
                        callback();
                    })
                },
            }, (error: any) => {
                if (error) {
                    res.status(500).json({
                        status: 500,
                        message: error
                    })
                } else {
                    res.status(200).json({
                        status: 200,
                        message: 'Archivo de coordenadas cargado correctamente',
                        nombreArchivo: nameFile
                    })
                }
            })
        } catch (error) {
            res.status(500).json({
                status: 500,
                message: error
            })
        }
    }

    // Metodo read de la clase FileServices
    public read(req: express.Request, res: express.Response): void {
        // Instancia de la clase Util
        const objUtil: Util = new Util()
        // Instancia de la clase Client para base de datos
        const client: Client = new Client({ connectionString: objUtil.connection() })
        // Conexion a base de datos
        client.connect((err) => {
            if (err) {
                return console.error('ERROR de conexion con base de datos', err)
            }
        })
        const listLatLon: any[] = []
        const listLatLonExitosa: any[] = []
        const listLatLonFallida: any[] = []
        const listaCodigoPostal: any[] = []
        const target: string = `./public/${req.body.nameFile}`
        // Manejo de Excepcion
        try {
            async.series({
                readFile: (callback0: any) => {
                    let listLatLonAux: any[] = []
                    // Lectura del archivo
                    const data = fs.readFileSync(target, {})
                    // Vector con registro de cada linea del archivo
                    listLatLonAux = data.toString().split('\n')
                    // Eliminacion de la linea 1 (nombre de las columnas o cabecera del archivo)
                    listLatLonAux.splice(0, 1)
                    async.forEachOf(listLatLonAux, async (element: any, index: any, callback1: any) => {
                        // Validacion para no procesar lineas vacias o sin coordenadas del archivo
                        if (element.toString().length !== 0) {
                            // console.log('AGREGADO ' + index + ' | ' + element) // TODO: DEBUG
                            listLatLon.push(element)
                            const coordenadas: any = {
                                lat: parseFloat(element.split(',')[0]),
                                lon: parseFloat(element.split(',')[1])
                            }
                            // Validacion para no procesar coordenadas en cero(0)
                            if (coordenadas.lat !== 0 && coordenadas.lon !== 0) {
                                const url: string = `${process.env.URL_API_POSTCODES_IO}?lon=${coordenadas.lon}&lat=${coordenadas.lat}`
                                try {
                                    // Llamada axios a la API
                                    const { data: codigoPostal } = await axios.get(url)
                                    // console.log('AXIOS ' + codigoPostal.status) // TODO: DEBUG
                                    // Validacion para solo procesar respuestas con statusCode 200 y result diferentes a null o undefiend
                                    if (codigoPostal.status === 200 && codigoPostal.result) {
                                        // console.log('API OK ' + index, coordenadas) // TODO: DEBUG
                                        listLatLonExitosa.push(coordenadas)
                                        let posicion: number = 0
                                        async.forEachOf(codigoPostal.result, (value: any, key: any, callback2: any) => {
                                            let resta: number = 99999999
                                            // Calculo para determinar exactitud de la coordenada de lectura con la obtenida de la API
                                            const diferencia: number = (coordenadas.lat - value.latitude) - (coordenadas.lon - value.longitude)
                                            // Validacion para determinar si el resultado de la diferencia es menor a la resta (inicializada en un numero alto para tomar siempre el primero)
                                            if (diferencia < resta) {
                                                resta = diferencia
                                                posicion = key
                                            }
                                            callback2()
                                        }, async (error: any) => {
                                            if (error) {
                                                res.status(500).json({
                                                    status: 500,
                                                    message: error
                                                })
                                            }
                                            // Agregado del elemento del array de result que mas se acerca a las coordenadas del archivo
                                            listaCodigoPostal.push(codigoPostal.result[posicion].postcode)
                                            const query = `INSERT INTO codigos_postales (postcode, outcode, incode, quality, eastings, northings, country, nhs_ha, admin_county, admin_district, admin_ward, longitude, latitude, parliamentary_constituency, european_electoral_region, primary_care_trust, region, parish, lsoa, msoa, ced, ccg, nuts, codes) VALUES ('${codigoPostal.result[posicion].postcode}', '${codigoPostal.result[posicion].outcode}', '${codigoPostal.result[posicion].incode}', ${codigoPostal.result[posicion].quality}, ${codigoPostal.result[posicion].eastings}, ${codigoPostal.result[posicion].northings}, '${codigoPostal.result[posicion].country ? codigoPostal.result[posicion].country.replace(/'/g,'`') : ''}', '${codigoPostal.result[posicion].nhs_ha ? codigoPostal.result[posicion].nhs_ha.replace(/'/g,'`') : ''}', '${codigoPostal.result[posicion].admin_county ? codigoPostal.result[posicion].admin_county?.replace(/'/g,'`') : ''}', '${codigoPostal.result[posicion].admin_district ? codigoPostal.result[posicion].admin_district.replace(/'/g,'`') : ''}', '${codigoPostal.result[posicion].admin_ward ? codigoPostal.result[posicion].admin_ward.replace(/'/g,'`') : ''}', ${codigoPostal.result[posicion].longitude}, ${codigoPostal.result[posicion].latitude}, '${codigoPostal.result[posicion].parliamentary_constituency ? codigoPostal.result[posicion].parliamentary_constituency.replace(/'/g,'`') : ''}', '${codigoPostal.result[posicion].european_electoral_region ? codigoPostal.result[posicion].european_electoral_region.replace(/'/g,'`') : ''}', '${codigoPostal.result[posicion].primary_care_trust ? codigoPostal.result[posicion].primary_care_trust.replace(/'/g,'`') : ''}', '${codigoPostal.result[posicion].region ? codigoPostal.result[posicion].region.replace(/'/g,'`') : ''}', '${codigoPostal.result[posicion].parish ? codigoPostal.result[posicion].parish.replace(/'/g,'`') : ''}', '${codigoPostal.result[posicion].lsoa ? codigoPostal.result[posicion].lsoa.replace(/'/g,'`'): ''}', '${codigoPostal.result[posicion].msoa ? codigoPostal.result[posicion].msoa.replace(/'/g,'`') : ''}', '${codigoPostal.result[posicion].ced ? codigoPostal.result[posicion].ced.replace(/'/g,'`') : ''}', '${codigoPostal.result[posicion].ccg ? codigoPostal.result[posicion].ccg.replace(/'/g,'`') : ''}', '${codigoPostal.result[posicion].nuts ? codigoPostal.result[posicion].nuts.replace(/'/g,'`') : ''}', '${JSON.stringify(codigoPostal.result[posicion].codes)}')`
                                            // console.log(query) // TODO: DEBUG
                                            // Registro en base de datos de la informacion que mas se acerca a las coordenadas del archivo
                                            await client.query(query)
                                        })
                                    } else {
                                        // console.log('API ERROR ' + index, element) // TODO: DEBUG
                                        listLatLonFallida.push(coordenadas)
                                    }
                                }
                                catch (error) {
                                    // console.log('API ERROR ' + error + ' ' + index, element) // TODO: DEBUG
                                    listLatLonFallida.push(coordenadas)
                                }
                            }
                        }
                        callback1()
                    }, (error: any) => {
                        if (error) {
                            res.status(500).json({
                                status: 500,
                                message: error
                            })
                        }
                        callback0()
                    })
                },
            }, (error: any) => {
                if (error) {
                    res.status(500).json({
                        status: 500,
                        message: error
                    })
                } else {
                    // console.log('PROCESAMIENTO COMPLETADO ' + listLatLon.length) // TODO: DEBUG
                    res.status(200).json({
                        status: 200,
                        listaCoordenadas: listLatLon,
                        totalCoordenadas: listLatLon.length,
                        listaCoordenadasExitosas: listLatLonExitosa,
                        totalCoordenadasExitosas: listLatLonExitosa.length,
                        listaCoordenadasFallidas: listLatLonFallida,
                        totalCoordenadasFallidas: listLatLonFallida.length,
                        listaCodigoPostalAPI: listaCodigoPostal,
                        totalCodigoPostalAPI: listaCodigoPostal.length,
                        message: 'Coordenadas procesadas correctamente'
                    })
                }
            })
        } catch (error) {
            res.status(500).json({
                status: 500,
                message: error
            })
        }
    }
}