/**
 * Copyright (C) 2020
 *
 * Autor: Francisco Gerardino
 *
 */

// Importacion de librerias
import axios from 'axios'
import { FileServices } from './file.services'
import { createRequest, createResponse } from 'node-mocks-http'

jest.mock('axios')

const valApiResponse = {
    data: {
        status: 200,
        result: [
            {
                postcode: "DE1 9RB",
                quality: 1,
                eastings: 435446,
                northings: 336356,
                country: "England",
                nhs_ha: "East Midlands",
                longitude: -1.474217,
                latitude: 52.923454,
                european_electoral_region: "East Midlands",
                primary_care_trust: "Derby City",
                region: "East Midlands",
                lsoa: "Derby 013F",
                msoa: "Derby 013",
                incode: "9RB",
                outcode: "DE1",
                parliamentary_constituency: "Derby South",
                admin_district: "Derby",
                parish: "Derby, unparished area",
                admin_county: null,
                admin_ward: "Arboretum",
                ced: null,
                ccg: "NHS Derby and Derbyshire",
                nuts: "Derby",
                codes: {
                    admin_district: "E06000015",
                    admin_county: "E99999999",
                    admin_ward: "E05001770",
                    parish: "E43000014",
                    parliamentary_constituency: "E14000663",
                    ccg: "E38000229",
                    ccg_id: "15M",
                    ced: "E99999999",
                    nuts: "UKF11",
                    lsoa: "E01013479",
                    msoa: "E02002808",
                    lau2: "E05001770"
                },
                distance: 0
            }
        ]
    }
}

const valApiResponseError = {
    data: {
        status: 500,
        result: null
    }
}

describe('FileServices', () => {
    // Verificacion de instancia de la clase FileServices
    test('Instancia correctamente de la clase FileServices', async done => {
        const obj: FileServices = new FileServices()
        expect(obj).toBeDefined()
        done()
    })

    // Verificacion del metodo upload de la clase FileServices
    test('Invocacion de metodo upload de la clase FileServices', async done => {
        const bodyAux = {
            archivo: "bDGLD_postcodesgeo.csv",
            name: "bDGLD_postcodesgeo.csv"
        }
        const req = createRequest({
            files: bodyAux,
            method: 'POST',
            url: '/file/upload'
        })
        const res = createResponse()
        const obj: FileServices = new FileServices()
        obj.upload(req, res)
        expect(obj).toBeDefined()
        done()
    })

    // Verificacion del metodo read de la clase FileServices
    test('Invocacion de metodo read de la clase FileServices con respuesta 200 de la API', async done => {
        const bodyAux = {
            nameFile: "bDGLD_postcodesgeo.csv"
        }
        const req = createRequest({
            body: bodyAux,
            method: 'POST',
            url: '/file/read'
        })
        const res = createResponse()
        axios.get = jest.fn().mockImplementation((url: string, data: any) => {
            return Promise.resolve(valApiResponse)
        })
        const obj: FileServices = new FileServices()
        obj.read(req, res)
        expect(obj).toBeDefined()
        done()
    })

    test('Invocacion de metodo read de la clase FileServices con respuesta diferente a 200 de la API', async done => {
        const bodyAux = {
            nameFile: "bDGLD_postcodesgeo.csv"
        }
        const req = createRequest({
            body: bodyAux,
            method: 'POST',
            url: '/file/read'
        })
        const res = createResponse()
        axios.get = jest.fn().mockImplementation((url: string, data: any) => {
            return Promise.resolve(valApiResponseError)
        })
        const obj: FileServices = new FileServices()
        obj.read(req, res)
        expect(obj).toBeDefined()
        done()
    })

    test('Invocacion de metodo read de la clase FileServices con respuesta fallida de la API', async done => {
        const bodyAux = {
            nameFile: "bDGLD_postcodesgeo.csv"
        }
        const req = createRequest({
            body: bodyAux,
            method: 'POST',
            url: '/file/read'
        })
        const res = createResponse()
        axios.get = jest.fn().mockImplementation((url: string, data: any) => {
            return Promise.reject('internal-server-error')
        })
        const obj: FileServices = new FileServices()
        obj.read(req, res)
        expect(obj).toBeDefined()
        done()
    })
})